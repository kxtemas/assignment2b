package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import beans.User;

@ManagedBean
public class FormController {

	public String onSubmit() {
		//get the user values from the input form
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//shows the user object data in the console log
		System.out.println("----------------------------------------------");
		System.out.println("the first name is " + user.getFirstName());
		System.out.println("the last name is " + user.getLastName());
	
		//put the user object into the POST request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//shows the next page
		return "TestResponse.xhtml";
	}
	public String onFlash(User user) {
		//get the user values from the input form
		FacesContext context = FacesContext.getCurrentInstance();
		//User user = ctx.getApplication().evaluateExpressionGet(ctx, "#{user}", User.class);
		//shows the user object data in the console log
		System.out.println("//////////////////////////////////////////////");
		System.out.println("the first name is " + user.getFirstName());
		System.out.println("the last name is " + user.getLastName());
		
		//put the user object into the POST request
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("user", user);
		//shows the next page
		return "TestResponse2.xhtml?faces-redirect=true";
		
			
			//ctx.getExternalContext().getFlash().put("user", user);
			
		}
	}

